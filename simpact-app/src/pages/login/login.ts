import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
<<<<<<< HEAD
import { SimpactProvider } from '../../providers/simpact/simpact';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NewUserPage } from '../new-user/new-user';
import { ImpactGamePage } from '../impact-game/impact-game';
import { MainGamePage } from '../main-game/main-game';

=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
<<<<<<< HEAD
  providers: [
      SimpactProvider
  ]
})
export class LoginPage {

	public username;
	public loading;
	private pass;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		private simpactProvider: SimpactProvider,
	    private storage: Storage,
	    private alertCtrl: AlertController,
	) {}

	ionViewDidEnter() {
		this.storage.get('user').then((val) => {
	      	if(val.isLoggedIn === true) this.navCtrl.push(MainGamePage,{},{animate:true, animation:'transition', duration:500, direction:'forward'});
	    })
	}

	presentAlert(message) {
	    let alert = this.alertCtrl.create({
	    	title: 'Error!',
			subTitle: message,
			buttons: ['Dismiss']
		    });
	    alert.present();
	}

  	login(){
	    this.simpactProvider.login(this.username, this.pass).subscribe(
	      data => {
	        const response = (data as any);
	        const objeto_retorno = JSON.parse(response._body);
	        let userObject = {
				'isLoggedIn': true,
				'token': objeto_retorno.token,
				'username': objeto_retorno.username,
				'novo': objeto_retorno.novo
	        };
	        this.storage.set('user', userObject);
	        if(objeto_retorno.novo === true) this.navCtrl.push(ImpactGamePage,{},{animate:true, animation:'transition', duration:500, direction:'forward'});
	        else this.navCtrl.push(MainGamePage,{},{animate:true, animation:'transition', duration:500, direction:'forward'});
	      },
	      error => {
	        this.presentAlert("Erro no login! Confira seu email ou sua senha!");
	      }
	    )
    }

    openNewUser(){
    	this.navCtrl.push(NewUserPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'});
    }
=======
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

}
