import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SimpactProvider } from '../../providers/simpact/simpact';
import { AlertController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ShareInvestmentsPage } from '../share-investments/share-investments';

/**
 * Generated class for the NextMonthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-first-month',
  templateUrl: 'first-month.html',
  providers: [
    SimpactProvider
  ]
})
export class FirstMonthPage {

	public acoes;
	public acao;
	private token;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public menuCtrl: MenuController,
	 	private storage: Storage,
  	private simpactProvider: SimpactProvider,
  	private alertCtrl: AlertController,
		) {
	}

  getStocks(){
  	this.storage.get('user').then((val) => {
  	  this.token = val.token;	
      this.simpactProvider.getAllStock(this.token).subscribe(
        data => {
          const response = (data as any);
          let variavel = JSON.parse(response._body);
          this.acoes = variavel.msg;
        },
        error => {
          console.log("Erro no login! Confira seu email ou sua senha!");
        }
      )
    })
  }

  showInfo(codigo){
  	this.simpactProvider.stockInfo(this.token, codigo).subscribe(
  		data => {
          const response = (data as any);
          this.acao = JSON.parse(response._body);
          this.presentAlert(this.acao[0]);

          // if(objeto_retorno.status === 409) this.presentAlert("Há um jogo ativo! Deseja começar um novo jogo?", val.token);
        },
        error => {
          console.log("Erro no login! Confira seu email ou sua senha!");
        }
  	)
  }

  openShareInvestments(pagina){
    this.navCtrl.push(ShareInvestmentsPage,{'pagina': pagina},{animate:true, animation:'transition', duration:500, direction:'forward'})
  }

  presentAlert(acao) {
    let alert = this.alertCtrl.create({
      title: acao.codigo,
      subTitle: `<ul>
      <li> Nome: `+ acao.nome +`</li>
      <li> Ramo: `+acao.ramo+`</li>
      <li> Tipo: `+acao.tipo+`</li>
      <li> Cidade: `+acao.cidade+`</li>
      </ul>`,
      buttons: [{
        text: 'Investir',
        handler: () => {
          this.openShareInvestments(acao);
        }
      },{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    alert.present();
  }

  ionViewDidLoad() {
    this.getStocks();
  }

}
