import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirstMonthPage } from './first-month';

@NgModule({
  declarations: [
    FirstMonthPage,
  ],
  imports: [
    IonicPageModule.forChild(FirstMonthPage),
  ],
})
export class FirstMonthPageModule {}
