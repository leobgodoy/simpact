import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvestProfilePage } from './invest-profile';

@NgModule({
  declarations: [
    InvestProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(InvestProfilePage),
  ],
})
export class InvestProfilePageModule {}
