import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvestAnswerPage } from './invest-answer';

@NgModule({
  declarations: [
    InvestAnswerPage,
  ],
  imports: [
    IonicPageModule.forChild(InvestAnswerPage),
  ],
})
export class InvestAnswerPageModule {}
