import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
<<<<<<< HEAD
import { Storage } from '@ionic/storage';
import { SimpactProvider } from '../../providers/simpact/simpact';
import { Slides } from 'ionic-angular';
import { ViewChild } from '@angular/core';
=======

/**
 * Generated class for the MethodSelectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

@IonicPage()
@Component({
  selector: 'page-method-select',
  templateUrl: 'method-select.html',
})
export class MethodSelectPage {

<<<<<<< HEAD
	public acoes;
    public codigo;
 	public pontos;
 	public choice;
 	public cotas;
 	public singleValue = 0;
	private token;

  	@ViewChild(Slides) slides: Slides;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		private simpactProvider: SimpactProvider,
		private storage: Storage,
	) {}

	getStocks(){
		this.storage.get('user').then((val) => {
			this.token = val.token;  
			this.simpactProvider.getAllStock(this.token).subscribe(
				data => {
					const response = (data as any);
					let variavel = JSON.parse(response._body);
					let acoes = variavel.msg.filter(function(e){return e.cotas < 1000})
					this.acoes = acoes;
				},
				error => {
					console.log("Erro no login! Confira seu email ou sua senha!");
				}
			)
		})
	}

	thisChoice(acao){
		this.choice = acao;
	    this.codigo = this.choice.codigo;
	    this.pontos = this.choice.pontos;
	    this.cotas = this.choice.cotas;
	    this.slides.lockSwipeToNext(false);
	    this.slides.slideTo(1, 200);
	    this.slides.lockSwipeToNext(true);
	}

	sellStock(cotas){
		this.simpactProvider.sellStock(this.token, this.codigo, cotas).subscribe(
			data => {
				const response = (data as any);
		        let msg = JSON.parse(response._body);
		        console.log(msg);
			},
			error => {
				console.log("Erro no login! Confira seu email ou sua senha!");
			}
		)
		this.slides.lockSwipeToNext(false);
	    this.slides.slideTo(2, 200);
	    this.slides.lockSwipeToNext(true);
	}

	ionViewDidLoad() {
		this.getStocks();
	}
=======
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MethodSelectPage');
  }
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

}
