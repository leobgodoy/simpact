import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MethodSelectPage } from './method-select';

@NgModule({
  declarations: [
    MethodSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(MethodSelectPage),
  ],
})
export class MethodSelectPageModule {}
