import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImpactGamePage } from './impact-game';

@NgModule({
  declarations: [
    ImpactGamePage,
  ],
  imports: [
    IonicPageModule.forChild(ImpactGamePage),
  ],
})
export class ImpactGamePageModule {}
