import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ImpactGamePage } from '../impact-game/impact-game';
import { SimpactProvider } from '../../providers/simpact/simpact';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the NewUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-user',
  templateUrl: 'new-user.html',
  providers: [
      SimpactProvider
  ]
})
export class NewUserPage {

  public nome;
  public sobrenome;
  public email;
  public telefone;
  public username;
  private pass;
  private repass;
  public loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private simpactProvider: SimpactProvider,
    private storage: Storage,
    private alertCtrl: AlertController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewUserPage');
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Error!',
      message: message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  openImpactGame(){
    if(this.pass === undefined || this.repass === undefined) this.presentAlert("Os campos de senha estão em branco!");
    else if(this.pass === "" || this.repass === "") this.presentAlert("O campo de senha está em branco!");
    else if (this.pass !== this.repass) this.presentAlert("As senhas informadas não estão iguais!");
    else{  
  		this.simpactProvider.newUser(this.nome, this.sobrenome, this.email, this.telefone, this.username, this.pass).subscribe(
        data => {
          const response = (data as any);
          const objeto_retorno = JSON.parse(response._body);
          if(objeto_retorno === 500) this.presentAlert("Erro ao criar o usuário! Confira todos os campos e não deixe nenhum sem preencher!");
          else{
            let userObject = {
              'isLoggedIn': true,
              'token': objeto_retorno.token,
              'nome': objeto_retorno.nome,
              'username': objeto_retorno.username
            };
            this.storage.set('user', userObject);
            this.navCtrl.push(ImpactGamePage,{},{animate:true, animation:'transition', duration:500, direction:'forward'});
          }
        },
        error => {
          this.presentAlert("Erro no login");
        }
      )
    }
	}

}
