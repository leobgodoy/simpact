import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
<<<<<<< HEAD
import { InvestQuestionsPage } from '../invest-questions/invest-questions';
import { Storage } from '@ionic/storage';
import { SimpactProvider } from '../../providers/simpact/simpact';
import { Slides } from 'ionic-angular';
import { ViewChild } from '@angular/core';
=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

/**
 * Generated class for the ShareInvestmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-share-investments',
  templateUrl: 'share-investments.html',
})
export class ShareInvestmentsPage {

<<<<<<< HEAD
  public pagina;
  public acoes;
  public choice;
  public cotas;
  public codigo;
  public pontos;
  public singleValue = 0;
  public dinheiro;
  private token;

  @ViewChild(Slides) slides: Slides;

  constructor(
    public navCtrl: NavController, 
    private navParams: NavParams,
    private storage: Storage,
    private simpactProvider: SimpactProvider,
    ) {
  }

  getStocks(){
    this.storage.get('user').then((val) => {
      this.token = val.token;  
      this.simpactProvider.getAllStock(this.token).subscribe(
        data => {
          const response = (data as any);
          let variavel = JSON.parse(response._body);
          let acoes = variavel.msg.filter(function(e){return e.cotas > 0})
          this.acoes = acoes;
        },
        error => {
          console.log("Erro no login! Confira seu email ou sua senha!");
        }
      )
    })
  }

  thisChoice(acao){
    this.choice = acao;
    this.codigo = this.choice.codigo;
    this.pontos = this.choice.pontos;
    this.cotas = this.choice.cotas;
    this.slides.lockSwipeToNext(false);
    this.slides.slideTo(1, 200);
    this.slides.lockSwipeToNext(true);
  }

  ionViewDidEnter() {
    this.storage.get('user').then((val) => {
      this.simpactProvider.selectGame(val.token).subscribe(
        data => {
          const response = (data as any);
          let game = JSON.parse(response._body);
          this.dinheiro = game.msg.dinheiro;
        },
        error => {
          console.log("Erro no login");
        }
      )
    })
    this.getStocks();
    this.slides.lockSwipeToNext(true);
    this.pagina = this.navParams.get('pagina');
    if(this.pagina !== undefined) console.log(this.pagina)
    else console.log("UHHHHHHUUUUUUUUUU");
  }

	mainGame(){
		this.navCtrl.push(InvestQuestionsPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
	}

  reInvest(){
    this.slides.lockSwipeToNext(false);
    this.slides.slideTo(0, 200);
    this.slides.lockSwipeToNext(true);
  }

  investStock(wat){
    this.simpactProvider.investStock(this.token, this.codigo, wat).subscribe(
      data => {
        const response = (data as any);
        let msg = JSON.parse(response._body);
        console.log(msg);
      },
      error => {
        console.log("Erro no login! Confira seu email ou sua senha!");
      }
    )
    this.slides.lockSwipeToNext(false);
    this.slides.slideTo(2, 200);
    this.slides.lockSwipeToNext(true);
=======
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShareInvestmentsPage');
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
  }

}
