import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
<<<<<<< HEAD
import { Storage } from '@ionic/storage';
import { SimpactProvider } from '../../providers/simpact/simpact';
import { ShareInvestmentsPage } from '../share-investments/share-investments';
import { HomePage } from '../home/home';
import { WithdrawMoneyPage } from '../withdraw-money/withdraw-money';
import { LoadingController } from "ionic-angular";
import { MenuController } from 'ionic-angular';
=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

/**
 * Generated class for the InvestQuestionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
<<<<<<< HEAD
	selector: 'page-invest-questions',
	templateUrl: 'invest-questions.html',
	providers: [
		SimpactProvider,
	]
})

export class InvestQuestionsPage {

	public name;
	public description;
	public empresa;
	public loading;
	public noticias;
	public empresanome;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		private storage: Storage,
		private simpactProvider: SimpactProvider,
		public menuCtrl: MenuController
	) {}

	openMenu() {
	   	this.menuCtrl.open();
	}

	openShareInvestments(){
		this.navCtrl.push(ShareInvestmentsPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
	}

	openWithdrawMoney(){
		this.navCtrl.push(WithdrawMoneyPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
	}

	loadNextMonth(){
		// this.navCtrl.push(NextMonthPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
	}

	presentLoadingDefault() {
		this.loading = this.loadingCtrl.create({
			content: 'Carregando Respostas...'
		});

		this.loading.present();
	}

	getQuestion() {
		this.storage.get('user').then((val) => {
			this.simpactProvider.getQuestion(val.token).subscribe(
				data => {
			        const response = (data as any);
			        let resposta = JSON.parse(response._body);
			        this.noticias = resposta.msg;
			        console.log(this.noticias);
			    },
		      	error => {
		        	console.log("Erro no login");
		      	}
		    );
			if(!val.isLoggedIn) this.navCtrl.push(HomePage,{},{animate:true, animation:'transition', duration:500, direction:'backward'});
		});
	}

	ionViewDidEnter() {
		this.presentLoadingDefault();
		this.getQuestion();
	    this.isLoggedIn();
	    this.storage.get('user').then((val) => {
			this.name = val.nome;
			this.empresa = val.empresa;
	    });
    	this.dismissLoading();
	}

  	dismissLoading(){
    	this.loading.dismiss();
  	}

	isLoggedIn(){
		this.storage.get('user').then((val) => {
			if(!val.isLoggedIn) this.navCtrl.push(HomePage,{},{animate:true, animation:'transition', duration:500, direction:'backward'});
		})
	}

	//sticky feature

}
=======
  selector: 'page-invest-questions',
  templateUrl: 'invest-questions.html',
})
export class InvestQuestionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvestQuestionsPage');
  }

}
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
