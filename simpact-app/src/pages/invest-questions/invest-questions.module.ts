import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvestQuestionsPage } from './invest-questions';

@NgModule({
  declarations: [
    InvestQuestionsPage,
  ],
  imports: [
    IonicPageModule.forChild(InvestQuestionsPage),
  ],
})
export class InvestQuestionsPageModule {}
