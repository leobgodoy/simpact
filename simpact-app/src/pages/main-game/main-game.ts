import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
<<<<<<< HEAD
import { InvestQuestionsPage } from '../invest-questions/invest-questions';
import { FirstMonthPage } from '../first-month/first-month';
import { MenuController } from 'ionic-angular';
import { SimpactProvider } from '../../providers/simpact/simpact';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

/**
 * Generated class for the MainGamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main-game',
  templateUrl: 'main-game.html',
<<<<<<< HEAD
  providers: [
    SimpactProvider
  ]
})
export class MainGamePage {

  constructor(
    	public navCtrl: NavController, 
    	public navParams: NavParams,
    	public menuCtrl: MenuController,
      private storage: Storage,
      private simpactProvider: SimpactProvider,
      private alertCtrl: AlertController,
  	) {
  }
	openMenu() {
   	this.menuCtrl.open();
	}

	beginGame(){
    // this.navCtrl.push(FirstMonthPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
    this.storage.get('user').then((val) => {
      this.simpactProvider.newGame(val.token).subscribe(
        data => {
          const response = (data as any);
          const objeto_retorno = JSON.parse(response._body);
          if(objeto_retorno.status !== 200) this.presentAlert("Há um jogo ativo! Deseja começar um novo jogo?", val.token);
          //else this.navCtrl.push(InvestQuestionsPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
          else this.navCtrl.push(FirstMonthPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
        },
        error => {
          console.log("Erro no login! Confira seu email ou sua senha!");
        }
      )
  	})
  }

 	ionViewDidLoad() {
    this.resumeGame();
  }

  resumeGame() {
    this.storage.get('user').then((val) => {
      this.simpactProvider.selectGame(val.token).subscribe(
        data => {
          const response = (data as any);
          const objeto_retorno = JSON.parse(response._body);
          if(objeto_retorno.status === 409) this.presentAlert("Há um jogo ativo! Deseja começar um novo jogo?", val.token);
        },
        error => {
          console.log("Erro no login! Confira seu email ou sua senha!");
        }
      )
    })
  }

  presentAlert(message, token?) {
    let alert = this.alertCtrl.create({
      title: 'AVISO',
      subTitle: message,
      buttons: [{
        text: 'Encerrar Jogo Atual',
        handler: () => {
          this.simpactProvider.finishGame(token).subscribe();

        }
      },
      {
        text: 'Continuar Jogo Atual',
        handler: () => {
          this.navCtrl.push(InvestQuestionsPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
        }
      }]
    });
    alert.present();
  }

  continueGame(token) {

  }
=======
})
export class MainGamePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainGamePage');
  }

>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
}
