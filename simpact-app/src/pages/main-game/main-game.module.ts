import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainGamePage } from './main-game';

@NgModule({
  declarations: [
    MainGamePage,
  ],
  imports: [
    IonicPageModule.forChild(MainGamePage),
  ],
})
export class MainGamePageModule {}
