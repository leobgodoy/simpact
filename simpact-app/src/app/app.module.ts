import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';

import { AccountSettingsPageModule } from '../pages/account-settings/account-settings.module';
import { ImpactGamePageModule } from '../pages/impact-game/impact-game.module';
import { InvestAnswerPageModule } from '../pages/invest-answer/invest-answer.module';
import { InvestProfilePageModule } from '../pages/invest-profile/invest-profile.module';
import { InvestQuestionsPageModule } from '../pages/invest-questions/invest-questions.module';
import { LoginPageModule } from '../pages/login/login.module';
import { MainGamePageModule } from '../pages/main-game/main-game.module';
import { MethodSelectPageModule } from '../pages/method-select/method-select.module';
import { ShareInvestmentsPageModule } from '../pages/share-investments/share-investments.module';
import { WithdrawMoneyPageModule } from '../pages/withdraw-money/withdraw-money.module';
<<<<<<< HEAD
import { NewUserPageModule } from '../pages/new-user/new-user.module';
import { FirstMonthPageModule } from '../pages/first-month/first-month.module';
import { HttpModule } from "@angular/http";
import { Camera } from '@ionic-native/camera';
import { IonicStorageModule } from '@ionic/storage';
=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072


import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HomePage } from '../pages/home/home';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    AccountSettingsPageModule,
    ImpactGamePageModule,
    InvestAnswerPageModule,
    InvestProfilePageModule,
    InvestQuestionsPageModule,
    LoginPageModule,
    MainGamePageModule,
    MethodSelectPageModule,
    ShareInvestmentsPageModule,
    WithdrawMoneyPageModule,
<<<<<<< HEAD
    NewUserPageModule,
    HttpModule,
    FirstMonthPageModule,
    IonicStorageModule.forRoot(),
=======

>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
<<<<<<< HEAD
    Camera,
=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
