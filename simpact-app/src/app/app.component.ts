import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
<<<<<<< HEAD
import { Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController } from 'ionic-angular';
import { SimpactProvider } from '../providers/simpact/simpact';
import { Storage } from '@ionic/storage';
import { Inject, ViewChild } from '@angular/core';
=======
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072

import { HomePage } from '../pages/home/home';
import { AccountSettingsPage } from '../pages/account-settings/account-settings';
import { ImpactGamePage } from '../pages/impact-game/impact-game';
import { InvestAnswerPage } from '../pages/invest-answer/invest-answer';
import { InvestProfilePage } from '../pages/invest-profile/invest-profile';
import { InvestQuestionsPage } from '../pages/invest-questions/invest-questions';
import { LoginPage } from '../pages/login/login';
import { MainGamePage } from '../pages/main-game/main-game';
import { MethodSelectPage } from '../pages/method-select/method-select';
import { ShareInvestmentsPage } from '../pages/share-investments/share-investments';
import { WithdrawMoneyPage } from '../pages/withdraw-money/withdraw-money';
<<<<<<< HEAD
import { FirstMonthPage } from '../pages/first-month/first-month';


@Component({
  templateUrl: 'app.html',
  providers: [
    SimpactProvider,
  ]
})

export class MyApp {

  @ViewChild(Nav) nav;
 
=======

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
  rootPage:any = HomePage;
  accountSettingsPage:any = AccountSettingsPage;
  impactGamePage:any = ImpactGamePage;
  investAnswerPage:any = InvestAnswerPage;
  investProfilePage:any = InvestProfilePage;
  investQuestionsPage:any = InvestQuestionsPage;
  loginPage:any = LoginPage;
  mainGamePage:any = MainGamePage;
  methodSelectPage:any = MethodSelectPage;
  shareInvestmentsPage:any = ShareInvestmentsPage;
  withdrawMoneyPage:any = WithdrawMoneyPage;
<<<<<<< HEAD
  firstMonthPage:any = FirstMonthPage;

  public usuario;
  public nome;
  public sobrenome;
  public dinheiro;
  public game;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private simpactProvider: SimpactProvider,
    private storage: Storage,
    ) {
    platform.ready().then(() => {
      this.loadInformation();
=======

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
<<<<<<< HEAD

  openMenu() {
   this.menuCtrl.open();
  }

  logout(){
    this.storage.set('user', '');
    //colocar um push para a pagina do logout
  }

  loadInformation(){
    this.storage.get('user').then((val) => {
      this.simpactProvider.seeUser(val.token).subscribe(
        data => {
          const response = (data as any);
          this.usuario = JSON.parse(response._body);
          this.nome = this.usuario.nome;
          this.sobrenome = this.usuario.sobrenome;
          this.simpactProvider.selectGame(val.token).subscribe(
            data => {
              const response = (data as any);
              this.game = JSON.parse(response._body);
              this.dinheiro = this.game.msg.dinheiro;
            }
          )
        },
        error => {
          console.log("Erro no login");
        }
      );
    })
  }

  openShareInvestments(){
    this.menuCtrl.close();
    this.nav.push(ShareInvestmentsPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
  }

  openWithdrawMoney(){
    this.menuCtrl.close();
    this.nav.push(WithdrawMoneyPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
  }

  openMethodSelect(){
    this.menuCtrl.close();
    this.nav.push(MethodSelectPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
  }

  openInvestQuestions(){
    this.menuCtrl.close();
    this.nav.push(InvestQuestionsPage,{},{animate:true, animation:'transition', duration:500, direction:'forward'})
  }
=======
>>>>>>> 072002e6f17c41354cf46cb8a7f713bbe199f072
}

