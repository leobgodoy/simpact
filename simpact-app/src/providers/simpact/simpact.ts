import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the SimpactProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SimpactProvider {

  data: any

  private baseApiPath = "http://localhost:3000";

  constructor(public http: Http) { }

  login(username, password) {
    return this.http.post(this.baseApiPath + `/usuarios/login`, {"username": username, "password": password})
  }

  newUser(nome, sobrenome, email, telefone, username, pass){
    return this.http.post(this.baseApiPath + `/usuarios/newuser`, {"nome": nome, "sobrenome": sobrenome, "email": email, "telefone": telefone, "username": username, "password": pass});   
  }

  updateUser(token, nome?, sobrenome?, email?, telefone?){
    return this.http.put(this.baseApiPath + '/usuarios/updateuser', {"nome": nome, "sobrenome": sobrenome, "email": email, "telefone": telefone});
  }

  // deleteUser(token, placaToken){
  //   return this.http.get(this.baseApiPath + '/usuarios/appget', {params: {"token": token, "placa": placaToken}});
  // }

  seeUser(token){
    return this.http.get(this.baseApiPath + '/usuarios/profile', {params: {"token": token}});
  }

  getQuestion(token){
    return this.http.get(this.baseApiPath + '/games/showevents', {params: {"token": token}});
  }
  
  newGame(token){
    return this.http.post(this.baseApiPath + '/games/newgame', {"token": token})
  }

  selectGame(token){
    return this.http.get(this.baseApiPath + '/games/selectgame', {params: {"token": token}});
  }

  finishGame(token){
    return this.http.put(this.baseApiPath + '/games/finishgame', { "token": token });
  }

  getStock(token){
    return this.http.get(this.baseApiPath + '/acoes/getstocks', {params: {"token": token}});
  }

  // getAllStock(token){
  //   return this.http.get(this.baseApiPath + '/acoes/getallstocks', {params: {"token": token}});
  // }

  getAllStock(token){
    return this.http.get(this.baseApiPath + '/games/getallstocks', {params: {"token": token}});
  }

  stockInfo(token, codigo){
    return this.http.get(this.baseApiPath + '/acoes/stockinfo', {params: {"token": token, "codigo": codigo}});
  }

  investStock(token, codigo, cotas){
    return this.http.put(this.baseApiPath + '/games/investstock', {params: {"token": token, "codigo": codigo, "cotas": cotas}})
  }

  sellStock(token, codigo, cotas){
    return this.http.put(this.baseApiPath + '/games/sellstock', {params: {"token": token, "codigo": codigo, "cotas": cotas}})
  }
}
