/**********************************************************
					R E Q U I R E S
**********************************************************/

const express = require('express');
const Usuario = require('../controllers/usuarioController');
const http = require('axios');
const multer = require('multer');
const path = require('path');

/**********************************************************
				D E C L A R A T I O N S
**********************************************************/

const router = express.Router();

/***********************************************************
					F U N C T I O N S
***********************************************************/

router.get('/userlist', function(req, res){
	Usuario.listAll(function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.post('/newuser', function(req, res){
	Usuario.novoUsuario(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp.msg);
	})
})

router.post('/adduseradmin', function(req, res){
	Usuario.adicionarUsuarioAdmin(req.body, function(resp){
		res.json(resp.msg);
	})
})

router.put('/updateuser', function(req, res){
	Usuario.alterarUsuario(req.body, function(resp){
		if(resp.status === 500) res.json("Internal Server Error: At Update User");
		else if(resp.status === 404) res.json("User not found: At Update User");
		else res.json(resp.msg);
	})
})	

router.delete('/deleteuser', function(req, res){ //OK, importante observar que a informação vem do req.query e não req.body
	Usuario.deletarUsuario(req.query.token, req.query.id, function(resp){
		if(resp.status === 500) res.json(resp.msg);
		else if(resp.status === 400) res.json(resp.msg);
		else res.json(resp.msg);
	})
})

router.put('/changepassword', function(req, res){
	Usuario.alterarSenha(req.body, function(resp){
		if(resp.status === 400) res.json(resp.msg);
		else if(resp.status === 404) res.json(resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/profile', function(req, res){
	// Usuario.carregarPerfil(req.headers, function(resp){
	Usuario.carregarPerfil(req.query, function(resp){
		if(resp.status === 404) res.json(resp.msg);
		else res.json(resp.msg);
	})
})

// router.post('/removeimage', function(req, res){
// 	let usuario = {
// 		'token': req.body.token,
// 	}
// 	Usuario.removerImagem(usuario, function(resp){ 
// 		if(resp.status === 403)	res.json("E-mail already registred: At Add User");
// 		else if(resp.status === 500) res.json("Internal Server Error: At Add User");
// 		else res.json(resp.msg);
// 	})
// })

// router.put('/changeaccinfo', function(req, res){
// 	let usuario = {
// 		'email': req.body.email,
// 		'nome': req.body.nome,
// 		'sobrenome': req.body.sobrenome,
// 		'telefonenum': req.body.telefonenum,
// 		'token': req.body.token,
// 	}
// 	Usuario.mudarInformacoes(usuario, function(resp){
// 		if(resp.status === 400) res.json(resp.msg);
// 		else if(resp.status === 404) res.json(resp.msg);
// 		else res.json(resp.msg);
// 	})
// })

// ********************************************
// 		U P L O A D    F U N C T I O N S
// *********************************************

// var storage = multer.diskStorage({
// 	destination: '/var/www/autto-website/admin/src/img/',
// 	filename: function(req,file,cb){
// 		let info = {'token': req.body.token, 'ext': path.extname(file.originalname)};	
// 		Usuario.salvarImagem(info, function(resp){
// 			cb(null, resp.image);
// 		});
// 	} 
// })

// var upload = multer({ 
// 	storage: storage,
// 	fileFilter: function (req, file, cb) {
// 		console.log(path.extname(file.originalname) === '.png')
//     	if (path.extname(file.originalname) !== '.jpg' && path.extname(file.originalname) !== '.png' && path.extname(file.originalname) !== '.gif' ) {
//     		return cb('Only images (.jpg, .png or .gif) are allowed');
//     	}
//     	else cb(null, true);
//     },
// 	limits:{fileSize: 250000}
// }).single('avatar');

// router.post('/upload', function(req, res){
// 	 upload(req, res, (err) => {
// 	 	if(err){
// 	 		console.log(err);
// 	 		return res.status(405).send({ error: err });
// 	 	} else {
// 	 		res.redirect("http://www.movve.com.br/account_Settings.html");
// 	 	}
// 	 })
// })		

/*********************************************
		A U T H    F U N C T I O N S
**********************************************/

router.post('/login', function(req,res){
	Usuario.logarUsuario(req.body, function(resp){
		if(resp.status !== 200)
			res.status(resp.status).json(resp.msg);
		else
			res.json(resp.user);
	})
})

// router.post('/authorization', function(req, res){
// 	let info = {'token': req.body.token, 'page': req.body.page}
// 	Usuario.autorizarUsuario(info, function(resp){
// 		if(resp.status !== 200)
// 			res.json(resp.info)
// 		else
// 			res.json(resp.info);
// 	})
// })

// /*********************************************
// 	E N T E R P R I S E    F U N C T I O N S
// **********************************************/

// router.get('/enterpriseprofile', function(req, res){
// 	Usuario.empresaPerfil(req.query.token, function(resp){
// 		if(resp.status !== 200) res.status(resp.status).json(resp.msg);
// 		else res.json(resp.msg);
// 	})
// })

module.exports = router;
