/**********************************************************
					R E Q U I R E S
**********************************************************/

const express = require('express');
const Game = require('../controllers/gameController');
const http = require('axios');
const multer = require('multer');
const path = require('path');

/**********************************************************
				D E C L A R A T I O N S
**********************************************************/

const router = express.Router();

/***********************************************************
					F U N C T I O N S
***********************************************************/

router.get('/allgames', function(req, res){
	Game.listarJogos(function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/selectgame', function(req, res){
	// Game.selecionarJogo(req.headers, function(resp){
	Game.selecionarJogo(req.query, function(resp){
		if(resp.status === 500) res.json(resp);
		else res.json(resp);
	})
})

router.get('/getallstocks', function(req, res){
	// Game.mostrarAcoes(req.headers, function(resp){
	Game.mostrarAcoes(req.query, function(resp){
		if(resp.status === 500) res.json(resp);
		else res.json(resp);
	})
})

router.post('/newgame', function(req, res){
	Game.novoJogo(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})	

router.put('/finishgame', function(req, res){
	Game.terminarJogo(req.body, function(resp){
		if(resp.status === 500) res.json("Internal Server Error: At Update User");
		else if(resp.status === 404) res.json("User not found: At Update User");
		else res.json(resp.msg);
	})
})		

router.delete('/deletegame', function(req, res){ //OK, importante observar que a informação vem do req.query e não req.body
	Game.deletarJogo(req.query, function(resp){
		if(resp.status === 500) res.json(resp);
		else if(resp.status === 400) res.json(resp);
		else res.json(resp.msg);
	})
})

router.put('/investstock', function(req, res){
	Game.investirAcao(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})

router.put('/sellstock', function(req, res){
	Game.venderAcao(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})

router.get('/showevents', function(req, res){
	Game.mostrarEventos(req.query, function(resp){
		if(resp.status === 500) res.json(resp);
		else res.json(resp);
	})
})

router.get('/testi', function(req, res){
	Game.avancarSemana(function(resp){
		res.json(resp);
	})
})

module.exports = router;
