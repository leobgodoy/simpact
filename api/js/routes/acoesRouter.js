/**********************************************************
					R E Q U I R E S
**********************************************************/

const express = require('express');
const Acoes = require('../controllers/acoesController');
const http = require('axios');
const multer = require('multer');
const path = require('path');

/**********************************************************
				D E C L A R A T I O N S
**********************************************************/

const router = express.Router();

/***********************************************************
					F U N C T I O N S
***********************************************************/

router.get('/getstocks', function(req, res){
	Acoes.listarAcoes(req.query, function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/stockinfo', function(req, res){
	Acoes.infoAcao(req.query, function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/getallstocks', function(req, res){
	Acoes.mostrarTodos(req.query, function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/selectstock', function(req, res){
	// Acoes.selecionarJogo(req.headers, function(resp){
	Acoes.selecionarAcao(req.query, function(resp){
		if(resp.status === 500) res.json(resp);
		else res.json(resp);
	})
})

router.post('/newstock', function(req, res){
	Acoes.novaAcao(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})

router.put('/changestock', function(req, res){
	Acoes.alterarAcao(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})

router.put('/investstock', function(req, res){
	Acoes.investirAcao(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})

router.put('/sellstock', function(req, res){
	Acoes.venderAcao(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp);
	})
})

router.delete('/deletestock', function(req, res){ //OK, importante observar que a informação vem do req.query e não req.body
	// Acoes.deletarAcao(req.headers, function(resp){
	Acoes.deletarAcao(req.query, function(resp){
		if(resp.status === 500) res.json(resp);
		else if(resp.status === 400) res.json(resp);
		else res.json(resp.msg);
	})
})		

module.exports = router;
