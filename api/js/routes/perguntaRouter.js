/**********************************************************
					R E Q U I R E S
**********************************************************/

const express = require('express');
const Pergunta = require('../controllers/perguntaController');
const http = require('axios');
const multer = require('multer');
const path = require('path');

/**********************************************************
				D E C L A R A T I O N S
**********************************************************/

const router = express.Router();

/***********************************************************
					F U N C T I O N S
***********************************************************/

router.get('/allquestions', function(req, res){
	Pergunta.listarPerguntas(function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/pickquestion', function(req, res){
	Pergunta.pegarPergunta(req.query, function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.post('/newquestion', function(req, res){
	Pergunta.novaPergunta(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp.msg);
	})
})

router.put('/editquestion', function(req, res){
	Pergunta.alterarPergunta(req.body, function(resp){
		if(resp.status === 500) res.json("Internal Server Error: At Update User");
		else if(resp.status === 404) res.json("User not found: At Update User");
		else res.json(resp.msg);
	})
})	

router.delete('/deletequestion', function(req, res){ //OK, importante observar que a informação vem do req.query e não req.body
	Pergunta.deletarUsuario(req.query.token, req.query.id, function(resp){
		if(resp.status === 500) res.json(resp.msg);
		else if(resp.status === 400) res.json(resp.msg);
		else res.json(resp.msg);
	})
})


// ********************************************
// 		U P L O A D    F U N C T I O N S
// *********************************************

// var storage = multer.diskStorage({
// 	destination: '/var/www/autto-website/admin/src/img/',
// 	filename: function(req,file,cb){
// 		let info = {'token': req.body.token, 'ext': path.extname(file.originalname)};	
// 		Pergunta.salvarImagem(info, function(resp){
// 			cb(null, resp.image);
// 		});
// 	} 
// })

// var upload = multer({ 
// 	storage: storage,
// 	fileFilter: function (req, file, cb) {
// 		console.log(path.extname(file.originalname) === '.png')
//     	if (path.extname(file.originalname) !== '.jpg' && path.extname(file.originalname) !== '.png' && path.extname(file.originalname) !== '.gif' ) {
//     		return cb('Only images (.jpg, .png or .gif) are allowed');
//     	}
//     	else cb(null, true);
//     },
// 	limits:{fileSize: 250000}
// }).single('avatar');

// router.post('/upload', function(req, res){
// 	 upload(req, res, (err) => {
// 	 	if(err){
// 	 		console.log(err);
// 	 		return res.status(405).send({ error: err });
// 	 	} else {
// 	 		res.redirect("http://www.movve.com.br/account_Settings.html");
// 	 	}
// 	 })
// })		

module.exports = router;
