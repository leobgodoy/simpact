/**********************************************************
					R E Q U I R E S
**********************************************************/

const express = require('express');
const Eventos = require('../controllers/eventosController');
const http = require('axios');
const multer = require('multer');
const path = require('path');

/**********************************************************
				D E C L A R A T I O N S
**********************************************************/

const router = express.Router();

/***********************************************************
					F U N C T I O N S
***********************************************************/

router.get('/allevents', function(req, res){
	Eventos.listarEventos(function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.get('/getevent', function(req, res){
	Eventos.pegarEvento(req.query, function(resp){
		if(resp.status === 500) res.json(resp.status, resp.msg);
		else res.json(resp.msg);
	})
})

router.post('/newevent', function(req, res){
	Eventos.novoEvento(req.body, function(resp){ 
		if(resp.status === 403)	res.json("E-mail already registred: At New User");
		else if(resp.status === 500) res.json(500);
		else res.json(resp.msg);
	})
})

router.put('/editevent', function(req, res){
	Eventos.alterarEvento(req.body, function(resp){
		if(resp.status === 500) res.json("Internal Server Error: At Update User");
		else if(resp.status === 404) res.json("User not found: At Update User");
		else res.json(resp.msg);
	})
})	

router.delete('/deleteevent', function(req, res){ //OK, importante observar que a informação vem do req.query e não req.body
	Eventos.deletarUsuario(req.query.token, req.query.id, function(resp){
		if(resp.status === 500) res.json(resp.msg);
		else if(resp.status === 400) res.json(resp.msg);
		else res.json(resp.msg);
	})
})


// ********************************************
// 		U P L O A D    F U N C T I O N S
// *********************************************

// var storage = multer.diskStorage({
// 	destination: '/var/www/autto-website/admin/src/img/',
// 	filename: function(req,file,cb){
// 		let info = {'token': req.body.token, 'ext': path.extname(file.originalname)};	
// 		Eventos.salvarImagem(info, function(resp){
// 			cb(null, resp.image);
// 		});
// 	} 
// })

// var upload = multer({ 
// 	storage: storage,
// 	fileFilter: function (req, file, cb) {
// 		console.log(path.extname(file.originalname) === '.png')
//     	if (path.extname(file.originalname) !== '.jpg' && path.extname(file.originalname) !== '.png' && path.extname(file.originalname) !== '.gif' ) {
//     		return cb('Only images (.jpg, .png or .gif) are allowed');
//     	}
//     	else cb(null, true);
//     },
// 	limits:{fileSize: 250000}
// }).single('avatar');

// router.post('/upload', function(req, res){
// 	 upload(req, res, (err) => {
// 	 	if(err){
// 	 		console.log(err);
// 	 		return res.status(405).send({ error: err });
// 	 	} else {
// 	 		res.redirect("http://www.movve.com.br/account_Settings.html");
// 	 	}
// 	 })
// })		

module.exports = router;
