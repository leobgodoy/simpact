const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var AcoesSchema = new Schema({
	codigo: String,
	nome: String,
	tipo: String,
	ramo: String,
	cidade: String,
	pontos: Number,
	ativo: Boolean,
	cotas: Number
});



module.exports = mongoose.model('Acoes', AcoesSchema);
