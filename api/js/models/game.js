const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var GameSchema = new Schema({
	id: String,
	date: Date,
	dinheiro: String,
	usuario: String,
	ativo: Boolean,
	turno: Number,
	acoes: Object,
	eventos: Object,
});



module.exports = mongoose.model('Game', GameSchema);
