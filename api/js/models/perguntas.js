const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var PerguntaSchema = new Schema({
	numero: Number, //ID
	descricao: String, //Descrição dos eventos
	consequencia: Number, //porcentagem sobre o valor monetário do usuário
	imagem: String, //imagem que aparecerá
});

module.exports = mongoose.model('Pergunta', PerguntaSchema);
