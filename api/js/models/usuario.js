const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

var UsuarioSchema = new Schema({
	id: String,
	username: {type : String, required: true},
	nome: {type : String, required: true},
	sobrenome: {type : String, required: true},
	email: {type : String, required: true},
	telefone: {type : String, required: true},
	permissao: String,
	novo: Boolean,
	foto: String,
	senha: {type : String, required: true},
	token: String,
	acoes: Object,
	capital: Number
});

UsuarioSchema.methods.gerarSenha = function(senha){
	return bcrypt.hashSync(senha, bcrypt.genSaltSync(10));
}

UsuarioSchema.methods.validarSenha = function(senha){
	if(senha === undefined) return {status: 400, msg: "Password blank"};
	let info = bcrypt.compareSync(senha, this.senha);
	if(info === true) return {status: 200, msg: true}
	else return {status: 400, msg: "Incorrect Password!"}
}

UsuarioSchema.methods.gerarToken = function(usuario){
	return jwt.sign({'email': usuario.email, 'nome': usuario.nome, 'sobrenome': usuario.sobrenome}, 'simpactuz3r');
}

//Alterar para o financeiro
UsuarioSchema.methods.verificarToken = function(token, page){
	const roles = ['Administrador', 'Usuario', 'Console'];
	const pages = {
		Dashboard: ['Administrador', 'Console'],
		Frota: ['Administrador', 'Console'],
		Manutencao: ['Usuario', 'Administrador'],
		Usuarios: ['Administrador', 'Console']
	}
	let info = jwt.decode(token, {complete: true});

	if(pages[page].indexOf(info.payload.permissao) > -1)
			return true;
		else
			return false;
}

module.exports = mongoose.model('Usuario', UsuarioSchema);
