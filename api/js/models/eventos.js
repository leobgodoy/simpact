const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var EventoSchema = new Schema({
	numero: Number, //ID
	descricao: String, //Descrição dos eventos
	tipo: String, //Global, Privado ou Segmento
	// nome: String, //Possível nomes para eventos
	ativo: Boolean,
	usado: Boolean,
	turno: Number,
	posouneg: String, //se o impacto será positivo ou negativo ou ambos
	segmento: String, //petrolifero, alimentício, etc
	impacto: Number, //porcentagem sobre o valor monetário do usuário
	imagem: String, //imagem que aparecer
});

module.exports = mongoose.model('Evento', EventoSchema);