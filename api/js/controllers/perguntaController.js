/**********************************************************
					R E Q U I R E S
**********************************************************/

const Pergunta = require('../models/perguntas.js');
const fs = require('fs');
const ObjectId = require('mongodb').ObjectId;

/***********************************************************
					F U N C T I O N S
***********************************************************/

exports.novaPergunta = function(perguntaEnviada, cb){
	Pergunta.findOne({'token': perguntaEnviada.token}, function(erro, userValid){
		if(erro) cb({status: 500, msg: 'Internal Server Error ' + erro});
		else{
			Pergunta.find({}, function(err, perguntas){
				if(err) cb({status: 500, msg: 'Internal Server Error ' + erro});
				else{
					let novaPergunta = new Pergunta();
					novaPergunta.numero = perguntas.length; //trocar para a variável de numeros
					novaPergunta.descricao = perguntaEnviada.descricao;
					novaPergunta.consequencia = perguntaEnviada.consequencia;
					novaPergunta.imagem = perguntaEnviada.imagem;
					novaPergunta.save(function(erro, novo){
						if(erro){
							cb({status: 500, msg: "Internal server error: Saving new car"});
						}
						else{
							cb({status: 200, msg: "Question Created: "+ novo});
						}
					})
				}
			})
			
		}
	})
}

exports.alterarPergunta = function(pergunta, cb){
	Pergunta.findOne({'token': pergunta.token}, function(erro, userValid){
		if(!userValid) cb("Not valid token");
		else{
			Pergunta.findOne({'numero': pergunta.numero}, function(erro, perguntaSel){
				if(erro) cb({status: 500, msg: 'Internal Server Error ' + erro});
				else if(perguntaSel === null){
					cb({status: 404, msg: 'User Not Found ' + perguntaSel});
				}
				else{
					perguntaSel.nome = pergunta.nome || perguntaSel.nome;
					perguntaSel.descricao = pergunta.descricao || perguntaSel.descricao;
					perguntaSel.consequencia = pergunta.consequencia || perguntaSel.consequencia;
					Pergunta.updateOne({'_id': perguntaSel._id}, 
						{$set: 
							{
								'numero' : perguntaSel.numero,
								'descricao' : perguntaSel.descricao,
								'consequencia' : perguntaSel.consequencia,
							} 
						}, function(erro, ok){
							if (erro){
								cb({status: 500, msg: 'Internal Server Error ' + erro});
							}
							else{
								cb({status: 200, msg: perguntaSel});
							}
						}
					)
				}
			})
		}
	})
}

exports.deletarPergunta = function(pergunta, callback){
	Pergunta.findOne({'token': pergunta.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Pergunta.findOne({'_id': pergunta.numero}, function(erro, perguntaSel){
				if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
				else if(perguntaSel === null){
					callback({status: 404, msg: 'User Not Found ' + perguntaSel});
				}
				else {
					perguntaSel.remove(function(error){
						if(!error){
							callback({status: 200, msg: 'Pergunta excluído com sucesso!'})
						}
						else{
							callback({status: 500, msg: "Internal error: " + erro })
						}
					})
				}
			})
		}
	})
}

exports.listarPerguntas = function(callback){
	Pergunta.find({}, null, null, function(erro,user){
		if(erro) callback({status: 500, msg: "Internal error: " + erro });
		else callback({status: 200, msg: user});
	})
}

exports.pegarPergunta = function(params, cb){
	Pergunta.find({}, null, null, function(erro, question){
		if(erro) cb({status: 500, msg: "Internal error: " + erro });
		else cb({status: 200, msg: question});
	})
}