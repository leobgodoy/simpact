/**********************************************************
					R E Q U I R E S
**********************************************************/

const Eventos = require('../models/eventos.js');
const fs = require('fs');
const ObjectId = require('mongodb').ObjectId;

/***********************************************************
					F U N C T I O N S
***********************************************************/

exports.novoEvento = function(novoEvento, cb){
	Eventos.findOne({'token': novoEvento.token}, function(erro, userValid){
		if(erro) cb({status: 500, msg: 'Internal Server Error ' + erro});
		else{
			Eventos.find({}, function(err, eventos){
				if(err) cb({status: 500, msg: 'Internal Server Error ' + erro});
				else{
					let newEvento = new Eventos();
					newEvento.numero = eventos.length; //trocar para a variável de numeros
					newEvento.descricao = novoEvento.descricao;
					newEvento.tipo = novoEvento.tipo;
					newEvento.impacto = novoEvento.impacto;
					newEvento.posouneg = novoEvento.posouneg;
					newEvento.ativo = true;
					newEvento.usado = true;
					// newEvento.segmento = novoEvento.segmento;
					// newEvento.empresanome = novoEvento.empresanome;
					// newEvento.imagem = novoEvento.imagem;
					newEvento.save(function(erro, novo){
						if(erro){
							cb({status: 500, msg: "Internal server error: Saving new car"});
						}
						else{
							cb({status: 200, msg: "Event created: "+ novo});
						}
					})
				}
			})
			
		}
	})
}

exports.alterarEvento = function(pergunta, cb){ //completar
	Eventos.findOne({'token': pergunta.token}, function(erro, userValid){
		if(!userValid) cb("Not valid token");
		else{
			Eventos.findOne({'numero': pergunta.numero}, function(erro, perguntaSel){
				if(erro) cb({status: 500, msg: 'Internal Server Error ' + erro});
				else if(perguntaSel === null){
					cb({status: 404, msg: 'User Not Found ' + perguntaSel});
				}
				else{
					perguntaSel.nome = pergunta.nome || perguntaSel.nome;
					perguntaSel.descricao = pergunta.descricao || perguntaSel.descricao;
					perguntaSel.consequencia = pergunta.consequencia || perguntaSel.consequencia;
					Eventos.updateOne({'_id': perguntaSel._id}, 
						{$set: 
							{
								'numero' : perguntaSel.numero,
								'descricao' : perguntaSel.descricao,
								'consequencia' : perguntaSel.consequencia,
							} 
						}, function(erro, ok){
							if (erro){
								cb({status: 500, msg: 'Internal Server Error ' + erro});
							}
							else{
								cb({status: 200, msg: perguntaSel});
							}
						}
					)
				}
			})
		}
	})
}

exports.deletarEvento = function(pergunta, callback){
	Eventos.findOne({'token': pergunta.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Eventos.findOne({'_id': pergunta.numero}, function(erro, perguntaSel){
				if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
				else if(perguntaSel === null){
					callback({status: 404, msg: 'User Not Found ' + perguntaSel});
				}
				else {
					perguntaSel.remove(function(error){
						if(!error){
							callback({status: 200, msg: 'Pergunta excluído com sucesso!'})
						}
						else{
							callback({status: 500, msg: "Internal error: " + erro })
						}
					})
				}
			})
		}
	})
}

exports.listarEvento = function(callback){
	Eventos.find({}, null, null, function(erro,user){
		if(erro) callback({status: 500, msg: "Internal error: " + erro });
		else callback({status: 200, msg: user});
	})
}

exports.pegarEvento = function(params, cb){
	Eventos.find({}, null, null, function(erro, question){
		if(erro) cb({status: 500, msg: "Internal error: " + erro });
		else cb({status: 200, msg: question});
	})
}