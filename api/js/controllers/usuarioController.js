/**********************************************************
					R E Q U I R E S
**********************************************************/

const Usuario = require('../models/usuario.js');
const fs = require('fs');
const ObjectId = require('mongodb').ObjectId;

/***********************************************************
					F U N C T I O N S
***********************************************************/

exports.novoUsuario = function(usuario, cb){
	Usuario.findOne({'email': usuario.email}, function(erro, userExist){
		if(userExist){
			cb({status: 403, msg: "E-mail already registred"}); 
		}
		else {
			let novoUsuario = new Usuario();
			//novoUsuario.numero = 1; //trocar para a variável de numeros
			novoUsuario.nome = usuario.nome;
			novoUsuario.sobrenome = usuario.sobrenome;
			novoUsuario.email = usuario.email;
			novoUsuario.telefone = usuario.telefone;
			novoUsuario.username = usuario.username;
			novoUsuario.permissao = "user";
			novoUsuario.foto = undefined;
			novoUsuario.novo = true;
			novoUsuario.token = novoUsuario.gerarToken(usuario);
			novoUsuario.senha = novoUsuario.gerarSenha(usuario.password);
			novoUsuario.save(function(erro, novo){
				if(erro){
					cb({status: 500, msg: "Internal server error: Saving new car"});
				}
				else{
					cb({status: 200, msg: novo});
				}
			})
		}
	})
}

exports.alterarUsuario = function(usuario, cb){
	Usuario.findOne({'token': usuario.token}, function(erro, userValid){
		if(!userValid) cb({status: 404, msg: "Not valid token"});
		else if(erro) cb({status: 500, msg: 'Internal Server Error ' + erro});
		else{
			newUsuario = {
				'nome': usuario.nome || userValid.nome,
				'sobrenome': usuario.sobrenome || userValid.sobrenome,
				'email': usuario.email || userValid.email,
			}
			userValid.token = userValid.gerarToken(newUsuario);
			Usuario.updateOne({'_id': userValid._id}, 
				{$set:
					{
						'nome': newUsuario.nome,
						'sobrenome': newUsuario.sobrenome,
						'email': newUsuario.email,
						'telefone': usuario.telefone || userValid.telefone,
						'token': userValid.token,
						'novo': false
					}
				}, function(erro, ok){
					if (erro){
						cb({status: 500, msg: 'Internal Server Error ' + erro});
					}
					else{
						cb({status: 200, msg: ok});
					}
				}
			)
		}
	})
}

exports.deletarUsuario = function(token, usuario, callback){
	Usuario.findOne({'token': token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Usuario.findOne({'_id': usuario}, function(erro, usuarioSel){
				if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
				else if(usuarioSel === null){
					callback({status: 404, msg: 'User Not Found ' + usuarioSel});
				}
				else {
					usuarioSel.remove(function(error){
						if(!error){
							callback({status: 200, msg: 'Usuario excluído com sucesso!'})
						}
						else{
							callback({status: 500, msg: "Internal error: " + erro })
						}
					})
				}
			})
		}
	})
}

exports.listAll = function(callback){
	Usuario.find({}, null, null, function(erro,user){
		if(erro) callback({status: 500, msg: "Internal error: " + erro });
		else callback({status: 200, msg: user});
	})
}

exports.alterarSenha = function(usuario, callback){
	Usuario.findOne({'token': usuario.token}, function(erro, usuarioSel){
		if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
		else if(usuarioSel === null){
			callback({status: 404, msg: 'User Not Found ' + usuarioSel});
		}
		else {
			let checkLogin = usuarioSel.validarSenha(usuario.anterior);
			if(checkLogin.status === 200){
				usuarioSel.senha = usuarioSel.gerarSenha(usuario.senha);
				usuarioSel.save(function(erro, ok){
					callback({status: 200, msg: "Password changed!"} )	
				})	
			} else {
				callback({status: 400, msg: 'Incorrect Password...'} )
			}
		}
	})
}

exports.carregarPerfil = function(usuario, callback){
	Usuario.findOne({'token': usuario.token}, function(erro, usuarioSel){
		if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
		else if(usuarioSel === null){
			callback({status: 404, msg: 'User Not Found ' + usuarioSel});
		}
		else {
			callback({status: 200, msg: usuarioSel});
		}
	})
}

// exports.salvarImagem = function(info, callback){
// 	Usuario.findOne({'token': info.token}, function(erro, usuario){
// 		if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
// 		else if(usuario === null){
// 			callback({status: 404, msg: 'User Not Found ' + usuario});
// 		}
// 		else{
// 			// if(usuario.image !== undefined) fs.unlinkSync('/home/leonardo/Desktop/Autto/Product/autto-website/admin/src/img/'+usuario.image);
// 			if(usuario.image !== undefined) fs.unlinkSync('/var/www/autto-website/admin/src/img/'+usuario.image);
// 			let str = usuario.nome.replace(" ", "%20");
// 			usuario.image = str + '-' + Date.now() + info.ext;
// 			usuario.save(function(erro, ok){
// 				if(ok) callback({status: 200, msg: "Profile Image Updated!", image: usuario.image} )		
// 				else callback({status: 500, msg: 'Internal Server Error, DB error: At SalvarImagem'})
// 			})
// 		}
// 	})
// }

// exports.removerImagem = function(info, callback){
// 	Usuario.findOne({'token': info.token}, function(erro, usuario){
// 		if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
// 		else if(usuarioSel === null){
// 			callback({status: 404, msg: 'User Not Found ' + usuarioSel});
// 		}
// 		else{
// 			fs.unlinkSync('/var/www/autto-website/admin/src/img/'+usuario.image);
// 			// fs.unlinkSync('/home/leonardo/Desktop/Autto/Product/autto-website/admin/src/img/'+usuario.image);
// 			usuario.image = undefined;
// 			usuario.save(function(erro, ok){
// 				if(ok) callback({status: 200, msg: "Profile Image deleted!"} )		
// 				else callback({status: 500, msg: 'Internal Server Error, DB error: At removerImagem'})
// 			})
// 		}
// 	})
// }


// exports.mudarInformacoes = function(usuario, callback){
// 	Usuario.findOne({'token':usuario.token}, function(erro, usuarioSel){
// 		if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
// 		else if(usuarioSel === null){
// 			callback({status: 404, msg: 'User Not Found ' + usuarioSel});
// 		}
// 		else{
// 			usuarioSel.nome = usuario.nome || usuarioSel.nome;
// 			usuarioSel.email = usuario.email || usuarioSel.email;
// 			usuarioSel.sobrenome = usuario.sobrenome || usuarioSel.sobrenome;
// 			usuarioSel.telefonenum = usuario.telefonenum || usuarioSel.telefonenum;
// 			newUsuario = {
// 				'nome': usuarioSel.nome, 
// 				'email': usuarioSel.email,
// 				'permissao': usuarioSel.permissao
// 			}
// 			usuarioSel.token = usuarioSel.gerarToken(newUsuario);
// 			Usuario.updateOne({'_id': usuarioSel._id}, {$set: {'nome': usuarioSel.nome, 'email' : usuarioSel.email, 'token': usuarioSel.token, 'sobrenome': usuarioSel.sobrenome, 'telefonenum': usuarioSel.telefonenum} }, function(erro, ok){
// 				if (erro){
// 					callback({status: 500, msg: 'Internal Server Error ' + erro});
// 				}
// 				else{
// 					callback({status: 200, msg: usuarioSel});
// 				}
// 			})	
// 		}
// 	})
// }

/***************************************************************
    			 	A U T H     C O N T R O L 
***************************************************************/

exports.logarUsuario = function(usuario, callback){
	Usuario.findOne({'username': usuario.username}, function(err, res){
		if(err) callback({status: 500, msg: 'Internal Server Error ' + err});
		else if(res === null){
			callback({status: 404, msg: 'User Not Found ' + res});
		}
		else {
			let checkLogin = res.validarSenha(usuario.password);
			if(checkLogin.status === 200){
				res.token = res.gerarToken(res);
				res.save(function(erro, ok){
					callback({status: 200, user: ok} )	
				})	
			} else {
				callback({status: 400, msg: 'Incorrect Password'} )
			}
		}
	})
}

exports.autorizarUsuario = function(info, callback){
	Usuario.findOne({'token': info.token}, function(erro, usuarioSel){
		if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
		else if(usuarioSel === null){
			callback({status: 404, msg: 'User Not Found ' + usuarioSel});
		}
		else{
			if(usuarioSel.verificarToken(usuarioSel.token, info.page) === true){
				callback({status: 200, msg: 'OK', info: true});
			} else{
				callback({status: 401, msg: 'Not Authorized', info: false} );
			}
		}
	})
}

