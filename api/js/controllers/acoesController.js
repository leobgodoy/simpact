/**********************************************************
					R E Q U I R E S
**********************************************************/

const Usuarios = require('../models/usuario.js');
const Acoes = require('../models/acoes.js');
const fs = require('fs');
const ObjectId = require('mongodb').ObjectId;

/***********************************************************
					F U N C T I O N S
***********************************************************/

exports.novaAcao = function(acao, cb){
	Usuarios.findOne({'email': acao.email}, function(erro, userExist){
		if(!userExist){
			cb({status: 403, msg: "Error at New Stock"}); 
		}
		else {
			let novaAcao = new Acoes();
			novaAcao.codigo = acao.codigo;
			novaAcao.nome = acao.nome;
			novaAcao.tipo = acao.tipo;
			novaAcao.ramo = acao.ramo;
			novaAcao.cidade = acao.cidade;
			novaAcao.ativo = true;
			novaAcao.save(function(erro, novo){
				if(erro){
					cb({status: 500, msg: "Internal server error: Saving new car"});
				}
				else{
					cb({status: 200, msg: novo});
				}
			})
		}
	})
}

exports.alterarAcao = function(acao, cb){
	Usuarios.findOne({'token': acao.token}, function(erro, userValid){
		if(!userValid) cb({status: 404, msg: "Not valid token"});
		else if(erro){
			cb({status: 500, msg: 'Internal Server Error ' + erro});
		}
		else{
			Acoes.findOne({'codigo': acao.codigo}, function(erro, stock){
				changeStock = {
					'codigo': acao.codigo || stock.codigo,
					'nome': acao.nome || stock.nome,
					'tipo': acao.tipo || stock.tipo,
					'ramo': acao.ramo || stock.ramo,
					'cidade': acao.cidade || stock.cidade,
				}
				Acoes.updateOne({'_id': stock._id}, 
					{$set: 
						{
							'codigo': changeStock.codigo,
							'nome': changeStock.nome,
							'tipo': changeStock.tipo,
							'ramo': acao.ramo,
							'token': userValid.token,
						}
					}, function(erro, ok){
						if (erro){
							cb({status: 500, msg: 'Internal Server Error ' + erro});
						}
						else{
							cb({status: 200, msg: ok});
						}
					}
				)
			})
		}
	})
}



exports.infoAcao = function(info, callback){
	Usuarios.findOne({'token': info.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Acoes.find({'codigo': info.codigo}, function(erro, stock){
				if(erro) callback({status: 500, msg: "Internal error: " + erro });
				else callback({status: 200, msg: stock});
			})
		}
	})
}

exports.deletarAcao = function(acao, callback){
	Usuarios.findOne({'token': acao.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Acoes.findOne({'codigo': acao.codigo}, function(erro, acaoSel){
				if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
				else if(acaoSel === null){
					callback({status: 404, msg: 'Stock Not Found ' + acaoSel});
				}
				else {
					acaoSel.remove(function(error){
						if(!error){
							callback({status: 200, msg: 'Acao excluído com sucesso!'})
						}
						else{
							callback({status: 500, msg: "Internal error: " + erro })
						}
					})
				}
			})
		}
	})
}

exports.listarAcoes = function(info, callback){
	Usuarios.findOne({'token': info.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Acoes.find({}, null, null, function(erro, stock){
				if(erro) callback({status: 500, msg: "Internal error: " + erro });
				else{
					howMuch = Math.floor(Math.random()*4 + 1);
					var aux = [];
					var cont = 0;
					while(cont < howMuch){
						aux += stock[Math.floor(Math.random()*stock.length)];
						cont++;
					}
					callback({status: 200, msg: aux});
				}
			})
		}
	})
}

exports.mostrarTodos = function(info, callback){
	Usuarios.findOne({'token': info.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Acoes.find({}, null, null, function(erro, stock){
				if(erro) callback({status: 500, msg: "Internal error: " + erro });
				else callback({status: 200, msg: stock});
			})
		}
	})
}