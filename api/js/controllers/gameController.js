/**********************************************************
					R E Q U I R E S
**********************************************************/

const Usuarios = require('../models/usuario.js');
const Game = require('../models/game.js');
const Acoes = require('../models/acoes.js');
const Eventos = require('../models/eventos.js')
const fs = require('fs');
const ObjectId = require('mongodb').ObjectId;

/***********************************************************
					F U N C T I O N S
***********************************************************/

exports.novoJogo = function(jogo, cb){
	Game.find({'usuario': jogo.token, 'ativo': true}, function(err, jogoAtivo){
		if(err) cb({status: 500, msg: 'Internal Server Error ' + err});
		else if(jogoAtivo.length !== 0) cb({status: 409 , msg: "Há um jogo ativo!"});
		else{
			/* continuar a partir daqui*/
			let novoJogo = new Game();
			// novoJogo.id = makeid(10);
			novoJogo.dinheiro = 10000;
			novoJogo.usuario = jogo.token;
			novoJogo.turno = 0;
			novoJogo.ativo = true;
			novoJogo.acoes = [];
			novoJogo.eventos = [];
			console.log(novoJogo);
			Acoes.find({'ativo': true}, function(err, acoes){
				for(let i = 0; i < acoes.length; i++){
					let stock = {}
					stock.codigo = acoes[i].codigo;
					stock.nome = acoes[i].nome;
					stock.tipo = acoes[i].tipo;
					stock.ramo = acoes[i].ramo;
					stock.cidade = acoes[i].cidade;
					stock.pontos = Math.floor((Math.random() * 100) + 1);
					stock.cotas = 1000;
					novoJogo.acoes[i] = stock;
				}
				// Eventos.find({}, function(err, eventos){
				// 	for(let j = 0; j < eventos.length; j++){
				// 		let events = {};
				// 		events.numero = eventos[j].numero;
				// 		events.descricao = eventos[j].descricao;
				// 		events.tipo = eventos[j].tipo;
				// 		events.nome = eventos[j].nome;
				// 		events.posouneg = eventos[j].posouneg;
				// 		events.segmento = eventos[j].segmento;
				// 		events.impacto = eventos[j].impacto;
				// 		events.imagem = eventos[j].imagem;
				// 		novoJogo.eventos[j] = events;
				// 	}
					// novoJogo.save(function(erro, novo){
					// 	if(erro){
					// 		cb({status: 500, msg: "Internal server error: Creating new game"});
					// 	}
					// 	else{
					// 		cb({status: 200, msg: "Game Created: "+ novo});
					// 	}
					// })
				// })
			})
		}
	})
}

exports.avancarSemana = function(cb){
	let general = Math.floor(Math.random() * 3) + 1;
	let private = Math.floor(Math.random() * 3) + 1;
	let segment = Math.floor(Math.random() * 3) + 1;
	let turno = [];
	let novoJogo = new Game();
	novoJogo.id = makeid(10);
	novoJogo.dinheiro = 10000;
	novoJogo.usuario = "aspokaspoksapokaspokaspoksa";
	novoJogo.turno = 0;
	novoJogo.ativo = true;
	novoJogo.acoes = [];
	Eventos.find({'ativo': true, 'tipo': "global"}, function(err, eventos){
		for(let j = 0; j < general; j++){
			let evento = {};
			evento.numero = eventos[j].numero;
			evento.descricao = eventos[j].descricao;
			evento.tipo = eventos[j].tipo;
			evento.nome = eventos[j].nome;
			evento.posouneg = eventos[j].posouneg;
			evento.segmento = eventos[j].segmento;
			evento.impacto = eventos[j].impacto;
			evento.imagem = eventos[j].imagem;
			turno[j] = evento;
		}
		Eventos.find({'ativo': true, 'tipo': "privado"}, function(err, privado){
			let marker = turno.length;
			for(let k = 0; k < private; k++){
				let evento = {};
				evento.numero = privado[k].numero;
				evento.descricao = privado[k].descricao;
				evento.tipo = privado[k].tipo;
				evento.nome = privado[k].nome;
				evento.posouneg = privado[k].posouneg;
				evento.segmento = privado[k].segmento;
				evento.impacto = privado[k].impacto;
				evento.imagem = privado[k].imagem;
				turno[marker++] = evento;
			}
			novoJogo.eventos = turno;


			/* terminar a configuração de eventos */

			
			// Eventos.find({'ativo': true, 'tipo': "segmento"}, function(err, segmento){
			// 	console.log(segmento)
			// 	let marker = turno.length--;
			// 	for(let i = 0; i < segment; i++){
			// 		let evento = {};
			// 		evento.numero = segmento[i].numero;
			// 		evento.descricao = segmento[i].descricao;
			// 		evento.tipo = segmento[i].tipo;
			// 		evento.nome = segmento[i].nome;
			// 		evento.posouneg = segmento[i].posouneg;
			// 		evento.segmento = segmento[i].segmento;
			// 		evento.impacto = segmento[i].impacto;
			// 		evento.imagem = segmento[i].imagem;
			// 		turno[marker++] = evento;
			// 	}
			// })
		})
	})
}

exports.terminarJogo = function(jogo, cb){
	Game.findOne({'usuario': jogo.token, 'ativo': true}, function(err, jogoAtivo){
		if(err) cb({status: 500, msg: 'Internal Server Error ' + err});
		else if(jogoAtivo === null) cb({status: 500, msg: 'Não existe jogo ativo!'})
		else {
			Game.updateOne({'_id': jogoAtivo._id}, 
				{$set: 
					{
						'ativo' : false,
					}
				}, function(erro, ok){
					if(erro) cb({status: 500, msg: 'Internal Server Error ' + erro});
					else cb({status: 200, msg: ok});
				}
			)
		}
	})
}

exports.deletarJogo = function(jogo, callback){
	Usuarios.findOne({'token': jogo.token}, function(erro, userValid){ //pode complementar com permissao
		if(!userValid) callback("Not valid token");
		else{
			Game.findOne({'_id': jogo.numero}, function(erro, jogoSel){
				if(erro) callback({status: 500, msg: 'Internal Server Error ' + erro});
				else if(jogoSel === null){
					callback({status: 404, msg: 'Game Not Found ' + jogoSel});
				}
				else {
					jogoSel.remove(function(error){
						if(!error){
							callback({status: 200, msg: 'Jogo excluído com sucesso!'})
						}
						else{
							callback({status: 500, msg: "Internal error: " + erro })
						}
					})
				}
			})
		}
	})
}

exports.listarJogos = function(callback){
	Game.find({}, null, null, function(erro, jogo){
		if(erro) callback({status: 500, msg: "Internal error: " + erro });
		else callback({status: 200, msg: jogo});
	})
}

exports.selecionarJogo = function(params, cb){
	Game.findOne({'usuario': params.token, 'ativo': true}, function(erro, jogo){
		if(erro) cb({status: 500, msg: "Internal error: " + erro });
		else if(jogo !== null) cb({ status: 409, msg: jogo}); 
		else console.log("eita");
	})
}

exports.mostrarAcoes = function(info, callback){
	Usuarios.findOne({'token': info.token}, function(erro, userValid){
		if(!userValid) callback("Not valid token");
		else{
			Game.findOne({'usuario': userValid.token, 'ativo': true}, function(erro, stock){
				if(erro) callback({status: 500, msg: "Internal error: " + erro });
				else callback({status: 200, msg: stock.acoes});
			})
		}
	})
}

exports.mostrarEventos = function(info, callback){
	Usuarios.findOne({'token': info.token}, function(erro, userValid){
		if(!userValid) callback("Not valid token");
		else{
			Game.findOne({'usuario': userValid.token}, function(erro, events){
				console.log(events);
				if(erro) callback({status: 500, msg: "Internal error: " + erro });
				else callback({status: 200, msg: events.eventos});
			})
		}
	})
}

function makeid(number) {
  	var text = "";
  	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  	for (var i = 0; i < number; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));

  	return text;
}

exports.investirAcao = function(acao, cb){
	Usuarios.findOne({'token': acao.params.token}, function(erro, userExist){
		if(!userExist) cb({status: 403, msg: "Error at New Stock"}); 
		else {
			Game.findOne({'usuario': userExist.token, 'ativo': true}, function(erro, gameFound){
				if(erro) console.log("oh shit");
				else{
					let index = gameFound.acoes.map(function(e) {return e.codigo}).indexOf(acao.params.codigo);
					let aPagar = gameFound.acoes[index].pontos * acao.params.cotas;
					let residuoUsuario = gameFound.dinheiro - aPagar;
					let residuoCotas = gameFound.acoes[index].cotas - acao.params.cotas;
					gameFound.acoes[index].cotas = residuoCotas;
					gameFound.dinheiro = residuoUsuario;
					Game.updateOne({'_id': gameFound._id},
						{$set:
							{
								'dinheiro': gameFound.dinheiro,
								'acoes': gameFound.acoes
							}
						}, function(erro, ok){
							if (erro){
								cb({status: 500, msg: 'Internal Server Error ' + erro});
							}
							else{
								cb({status: 200, msg: gameFound});
							}
						}
					)
				}
			})
		}
	})
}

exports.venderAcao = function(acao, cb){
	Usuarios.findOne({'token': acao.params.token}, function(erro, userExist){
		if(!userExist) cb({status: 403, msg: "Error at New Stock"}); 
		else {
			Game.findOne({'usuario': userExist.token, 'ativo': true}, function(erro, gameFound){
				if(erro) console.log("oh shit");
				else{
					let index = gameFound.acoes.map(function(e) {return e.codigo}).indexOf(acao.params.codigo);
					let aPagar = gameFound.acoes[index].pontos * acao.params.cotas;
					let residuoUsuario = Number(gameFound.dinheiro) + Number(aPagar);
					let residuoCotas = Number(gameFound.acoes[index].cotas) + Number(acao.params.cotas);
					gameFound.acoes[index].cotas = residuoCotas;
					gameFound.dinheiro = residuoUsuario;
					Game.updateOne({'_id': gameFound._id},
						{$set:
							{
								'dinheiro': gameFound.dinheiro,
								'acoes': gameFound.acoes
							}
						}, function(erro, ok){
							if (erro){
								cb({status: 500, msg: 'Internal Server Error ' + erro});
							}
							else{
								cb({status: 200, msg: gameFound});
							}
						}
					)
				}
			})
		}
	})
}