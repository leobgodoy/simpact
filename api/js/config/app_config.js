const express = require ('express');
const bodyParser = require('body-parser');
const port = '3000'; //porta que vai escutar

const app = module.exports = express();

app.listen(port); //app = nome do arquivo objeto/listen = a função que permite testar a aplicação/port = a porta que vai ir a aplicação

app.use(bodyParser.urlencoded({extended:true})); ////configurar o programa para receber um url
app.use(bodyParser.json()); //configurar o programa para receber um json
app.use(function(req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*'); //permite que qualquer aplicação acesse o API, * = coringa ou seja qualquer pode acessar
	res.setHeader('Access-Control-Allow-Methods', 'GET,POST, PUT, DELETE'); //diz quais métodos podem ser acessados
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization'); //autoriza o api quem tiver token
	next();
});