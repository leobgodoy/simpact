const mongoose = require('mongoose');

const urlString = 'mongodb://localhost:27017/simpact'; //String de conexão

mongoose.Promise = global.Promise;
mongoose.connect(urlString, function(err,res){
	if(err){
		console.log('Não foi possível conectar a: '+ urlString);
	}
	else{
		console.log('Conectado a: '+ urlString);
	}
});
