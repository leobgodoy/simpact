const express = require('express');
const bodyParser = require('body-parser');
const port = '3000';
const db = require('./js/config/db_config');
const Usuarios = require('./js/routes/usuarioRouter.js');
const Perguntas = require('./js/routes/perguntaRouter.js');
const Games = require('./js/routes/gameRouter.js');
const Acoes = require('./js/routes/acoesRouter.js');
const Eventos = require('./js/routes/eventosRouter.js');
const app = express();

app.use(bodyParser.urlencoded({extended:true})); //configurar o programa para receber um url
app.use(bodyParser.json()); //configurar o programa para receber um json
app.use(bodyParser.text({ type: 'text/plain' }));
app.use(function(req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*'); //permite que qualquer aplicação acesse o API, * = coringa ou seja qualquer pode acessar
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'); //diz quais métodos podem ser acessados
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization'); //autoriza o api quem tiver token
	next();
});

app.get('/', function(req,res){  //req= requisição / res = response
	res.end('Bem-vindo ao Simpact');
})

app.use('/usuarios', Usuarios);
app.use('/perguntas', Perguntas);
app.use('/games', Games);
app.use('/acoes', Acoes);
app.use('/eventos', Eventos);

app.listen(port);
